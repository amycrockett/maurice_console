package com.example.dependabucket_testing_gradle02

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DependabucketTestingGradle02Application {

	static void main(String[] args) {
		SpringApplication.run(DependabucketTestingGradle02Application, args)
	}

}
